# Numérique et société

## Contexte

Ce dépôt contient deux présentations autour de la place du numérique dans la société. L'intention est la même : rappeler l'omniprésence du numérique, au service d'une surveillance de masse et d'une économie de l'attention, hors de tout contrôle démocratique et à l'abri des regards.

* La [version courte](./version_courte/) a été conçue à partir de coupures de presse pour présenter ces enjeux rapidement et donner des pistes d'alternatives génériques. Elle a été présentée en introduction d'une Api/casoft.
* La [version longue](./version_longue/) fonctionne sur le même principe mais est beaucoup plus travaillée et fait une place aux impacts environnementaux du numérique. Elle est aussi plus interactive, avant et après. Elle a été présentée lors d'une rentrée sociétale.

Dans tous les cas, l'intention première est de s'appuyer sur un faisceau d'exemples marquants pour illustrer des concepts un peu vagues comme le capitalisme de surveillance. On ne rentre jamais dans le détail des mécanismes sous-jacents, qui font l'objet de conférences plus spécialisées (cf Technopolice ou Pandas et algorithmes, sur https://tube.picasoft.net).

## Pré-requis

Firefox.

Installer ce module : https://addons.mozilla.org/fr/firefox/addon/ignore-x-frame-options-header/
Dans `about:config`, passer `security.fileuri.strict_origin_policy` à `false`.

C'est du hack, à retirer après la présentation.