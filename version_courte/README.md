# Numérique et société

## Contexte

Slides pour une présentation Picasoft lors des API E22, basée sur des exemples plus ou moins d'actualité des problématiques que posent la centralisation, la complexification et la privatisation du numérique.

Picasoft est présentée comme un mini-modèle alternatif.

Durée estimée: 30 minutes.
Pas encore de texte, mais nécessaire pour se ré-approprier la présentation (TODO).

## Afficher

Les slides sont faites avec [reveal.js](https://revealjs.com/).

Ouvrir `index.html` dans Firefox. Naviguer avec les flèches (bas pour avancer dans la sous-section, droite pour changer de section).