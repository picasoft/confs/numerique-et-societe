# Picasoft
----

<img src="./img/picasoft_logo.png" width="20%">

### Quel est leur projet
### Quelles sont leurs méthodes
---

# Numérique et société
Le numérique est omniprésent et il transforme la société en profondeur, hors de tout contrôle démocratique.
----

## Caractéristiques
<img src="./img/factors.png" width="100%">
---

# Perte de contrôle
<img src="./img/surveillance.png" width="80%">

----

## Surveillance de masse × USA

<iframe class="article-frame" data-src="https://fr.wikipedia.org/wiki/XKeyscore"></iframe>
<a class="text-xs" href="https://fr.wikipedia.org/wiki/XKeyscore">https://fr.wikipedia.org/wiki/XKeyscore</a>
----

## Surveillance de masse × FR

<iframe class="article-frame" data-src="https://www.laquadrature.net/2013/12/03/vers-une-surveillance-generalisee-dinternet-en-france/"></iframe>
<a class="text-xs" href="https://www.laquadrature.net/2013/12/03/vers-une-surveillance-generalisee-dinternet-en-france">https://www.laquadrature.net/2013/12/03/vers-une-surveillance-generalisee-dinternet-en-france</a>
----

## Smart-cities

<iframe class="article-frame" data-src="https://technopolice.fr/villes/"></iframe>
<a class="text-xs" href="https://technopolice.fr/villes">https://technopolice.fr/villes</a>
----

## Vulnérabilités

<iframe class="article-frame" data-src="https://meltdownattack.com/"></iframe>
<a class="text-xs" href="https://meltdownattack.com">https://meltdownattack.com</a>
----

## Hacking silencieux

<iframe class="article-frame" data-src="https://www.amnesty.fr/liberte-d-expression/actualites/surveillance-revelations-sur-le-logiciel-espion-israelien-pegasus-nso-group"></iframe>
<a class="text-xs" href="https://www.amnesty.fr/liberte-d-expression/actualites/surveillance-revelations-sur-le-logiciel-espion-israelien-pegasus-nso-group">https://www.amnesty.fr/liberte-d-expression/actualites/surveillance-revelations-sur-le-logiciel-espion-israelien-pegasus-nso-group</a>
----

## IA × réseaux sociaux

<iframe class="article-frame" data-src="https://www.francetvinfo.fr/internet/reseaux-sociaux/twitter/tay-le-robot-de-microsoft-quitte-twitter-apres-des-derapages-racistes_1374963.html"></iframe>
<a class="text-xs" href="https://www.francetvinfo.fr/internet/reseaux-sociaux/twitter/tay-le-robot-de-microsoft-quitte-twitter-apres-des-derapages-racistes_1374963.html">https://www.francetvinfo.fr/internet/reseaux-sociaux/twitter/tay-le-robot-de-microsoft-quitte-twitter-apres-des-derapages-racistes_1374963.html</a>
----

## IA × justice

<iframe class="article-frame" data-src="https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing"></iframe>
<a class="text-xs" href="https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing">https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing</a>
---

# Data × profits
<img src="./img/manipulation.png" width="80%">
----

## Principe

<img src="./img/data.png" width="90%">

----

## Clickbait récompensé

<iframe class="article-frame" data-src="https://www.technologyreview.com/2021/11/20/1039076/facebook-google-disinformation-clickbait/"></iframe>
<a class="text-xs" href="https://www.technologyreview.com/2021/11/20/1039076/facebook-google-disinformation-clickbait">https://www.technologyreview.com/2021/11/20/1039076/facebook-google-disinformation-clickbait</a>
----

## Mécaniques toxiques

<iframe class="article-frame" data-src="https://www.wsj.com/articles/facebook-knows-instagram-is-toxic-for-teen-girls-company-documents-show-11631620739"></iframe>
<a class="text-xs" href="https://www.wsj.com/articles/facebook-knows-instagram-is-toxic-for-teen-girls-company-documents-show-11631620739">https://www.wsj.com/articles/facebook-knows-instagram-is-toxic-for-teen-girls-company-documents-show-11631620739</a>
----

## Colère valorisée

<iframe class="article-frame" data-src="https://www.washingtonpost.com/technology/2021/10/26/facebook-angry-emoji-algorithm/"></iframe>
<a class="text-xs" href="https://www.washingtonpost.com/technology/2021/10/26/facebook-angry-emoji-algorithm">https://www.washingtonpost.com/technology/2021/10/26/facebook-angry-emoji-algorithm</a>
----

## Discriminations

<iframe class="article-frame" data-src="https://www.theguardian.com/technology/2020/mar/17/tiktok-tried-to-filter-out-videos-from-ugly-poor-or-disabled-users"></iframe>
<a class="text-xs" href="https://www.theguardian.com/technology/2020/mar/17/tiktok-tried-to-filter-out-videos-from-ugly-poor-or-disabled-users">https://www.theguardian.com/technology/2020/mar/17/tiktok-tried-to-filter-out-videos-from-ugly-poor-or-disabled-users</a>
----

## Mise en danger

<iframe class="article-frame" data-src="https://www.vice.com/en/article/m7vzjb/location-data-abortion-clinics-safegraph-planned-parenthood"></iframe>
<a class="text-xs" href="https://www.vice.com/en/article/m7vzjb/location-data-abortion-clinics-safegraph-planned-parenthood">https://www.vice.com/en/article/m7vzjb/location-data-abortion-clinics-safegraph-planned-parenthood</a>
---

# Capitalisme de surveillance

----
<img src="./img/capitalism.png" width="50%">

La surveillance au sens large est intrinsèquement liée au numérique "industriel" et génère des profits colossaux.
----

## Sauf que...

<iframe class="article-frame" data-src="https://en.wikipedia.org/wiki/IBM_and_the_Holocaust"></iframe>
<a class="text-xs" href="https://en.wikipedia.org/wiki/IBM_and_the_Holocaust">https://en.wikipedia.org/wiki/IBM_and_the_Holocaust</a>
----

## Sauf que...

<iframe class="article-frame" data-src="https://www.dailymail.co.uk/news/article-9986225/Facebook-exempts-secret-whitelisted-elite-rules-allows-post-banned-content.html"></iframe>
<a class="text-xs" href="https://www.dailymail.co.uk/news/article-9986225/Facebook-exempts-secret-whitelisted-elite-rules-allows-post-banned-content.html">https://www.dailymail.co.uk/news/article-9986225/Facebook-exempts-secret-whitelisted-elite-rules-allows-post-banned-content.html</a>
----

## Sauf que...

<iframe class="article-frame" data-src="https://www.theguardian.com/technology/2018/sep/18/google-china-dragonfly-search-engine"></iframe>
<a class="text-xs" href="https://www.theguardian.com/technology/2018/sep/18/google-china-dragonfly-search-engine">https://www.theguardian.com/technology/2018/sep/18/google-china-dragonfly-search-engine</a>

---

# Éléments de réponse
----
<img src="./img/howto.png" width="50%">

Changer radicalement le modèle du numérique, en s'appuyant sur l'échelle locale, la transparence et le contrôle démocratique.
----

## Picasoft

* Services web libres et gratuits (Mattermost, pads, Kanban...)
* Sensibilisation (émission de radio, conférences, ateliers...)
* Formations (autodéfense numérique, APIs...)
* Gouvernance horizontale et taille humaine.

<img src="./img/chatons.png" width="20%">
----

## Libère ton PC

Tu as le droit de garder Windows après l'API, on t'en voudra pas! Mais au moins, tu auras un peu plus le choix, un peu plus d'autonomie.

<img src="./img/api.png" width="20%">
----

## Rejoins nous

Si ça te parle : picasoft@assos.utc.fr