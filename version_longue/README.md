# Numérique et société : hors contrôle

Voir [le README principal](../README.md) pour des considérations générales.

Ci-dessous se trouvent quelques infos sur la conférence. Il n'y a pas encore de script mais des notes de présentation qui accompagnent le diapo (*speaker notes*) grâce à un [plugin Reveal.js](https://revealjs.com/speaker-view/).

## Métadonnées

> Titre : Numérique et Société - Hors contrôle

### Intentions
* Comprendre le modèle économique des géants du numérique 
* Présenter le concept de capitalisme de surveillance
* Mettre en lumière les mécanismes de captation et leurs conséquences
* Moyens de luttes et alternatives:
    * Au niveau micro et méso : présentation et discussion
    * Au niveau macro : questionnements

### Non-intentions
* Juger ou moraliser les comportements individuels
* Donner notre opinion (les opinions sont signalées à l'oral ou par un émoji)

### Plan de l'intervention [2h]

#### Débat mouvant [20m]

L'intervention commence par un [débat mouvant](https://www.metacartes.cc/faire-ensemble/recettes/debat-mouvant/) sur la question suivante :

> Faut-il créer des GAFAM européens ?

D'expérience, cette question divise assez bien le "public Picasoft". L'intention est d'engager les corps et de faire brise-glace. Les opinions sur cette question pourraient aussi être amenées à changer au vu de la présentation, et elle ne nécessite pas de connaissances techniques.

À l'issue de 10 minutes de débat, les groupes (a priori deux : pour ou contre, même si dans un continuum) présentent en quelques mots les **2 arguments les plus forts**, puis la présentation commence.

#### Plan de la présentation [1h]

Les durées sont à valider "en vrai".

1. Le modèle économique des géants du numérique [10m]
2. Les mécanismes nécessaires pour qu'ils tiennent [5m]
3. Raisons de l'absence de régulation par les États [10m]
4. Concept de capitalisme de surveillance : il fait système [5m]
5. Conséquences de ce mode de société [15m]
6. Luttes et alternatives [15m]

#### Cercle samoan [40m]

Le [cercle samoan](https://www.metacartes.cc/faire-ensemble/recettes/cercle-samoan/) est une modalité de discussion/débat qui facilite le passage de la parole. Il y a toujours une place de libre pour discuter et l'engagement dans la discussion est matérialisée physiquement par un déplacement corporel, dans la même lignée que le débat mouvant.

L'intention est de re-mobiliser des éléments de la présentation pour imaginer des alternatives. Nous proposons 4 thèmes, avec une visée moyenne de 10 minutes par thème. Les animateur·ices font attention au temps.

Les 4 thèmes sont :
* Métaux rares : comment décolonialiser le numérique, en gros...
* Gouvernance : comment regagner en autonomie, comment re-démocratiser la question du numérique...
* Décentralisation (souhaitable ou non, possible ou non...) → rejoint la question du débat mouvant.
* Comment les participant·es se voient dans leur futur
    * Type de travail
    * Type de structure (est-ce qu'au lieu de créer des trucs à côté on devrait pas vraiment se poser la question du redressement des GAFAMs ?)
    * Utilisation personnelle
    * Comment ils sont au niveau militantisme, projets prévus ?

### Guidelines pour la préparation
* Une slide = une idée + le plus souvent une image d'illustration
* Opinion = emoji visible ou signalement oral
* Affirmation =
    * Pour une actualité : journal "reconnu"
    * Pour une généralisation : article scientifique

## Lancer la présentation

Ce plugin nécessite de lancer un [serveur local Reveal.js](https://revealjs.com/installation/#full-setup). Pour ne pas saturer le dépôt en clonant tout Reveal, il est intégré grâce à un [sous-module Git](https://www.git-scm.com/book/en/v2/Git-Tools-Submodules).

Après avoir cloné ce dépôt, lancer la commande suivante pour récupérer Reveal :

```bash
git submodule update --init --recursive
```

Pour mettre à jour le sous-module, utiliser :

```bash
git submodule update --recursive
```

Ensuite, installer [Node.js](https://nodejs.org) puis les dépendances :

```bash
cd reveal
npm install
```

Créer un lien symbolique de la présentation dans le sous-module reveal :

```bash
ln -s ../pres ./pres
```

Enfin, lancer le serveur local :

```bash
npm start
```

Puis visiter [localhost:8000/pres](http://localhost:8000/pres) pour voir la présentation.

## Utiliser la présentation

Appuyer sur `S`.

La présentation s'ouvre dans deux fenêtres :
* Une qui contient la slide actuelle, en plein écran → à projeter
* Une qui contient les notes, une prévisualisation de la slide suivante ainsi que le temps écoulé → à garder sur un écran posé sur un bureau par exemple.

Les flèches du haut et du bas permettent de naviguer dans les sous-sections tandis que les flèches de gauche et de droite permettent de naviguer dans les sections.

* La touche `B` permet de mettre en pause la présentation.
* La touche `Esc` permet de dézoomer et de voir toutes les slides.
* La touche `?` affiche l'aide.

## Modifier la présentation

Tout est dans [pres/index.html](./pres/index.html). Contrairement à la version courte, on utilise du HTML directement pour simplifier les opérations (le Markdown est une fausse bonne idée quand on veut ajouter des effets avancés).

Voir la doc de Reveal : https://revealjs.com